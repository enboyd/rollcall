package com.enboyd.rollcall.dao;

import com.enboyd.rollcall.entity.Student;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.*;

/**
 * ProjectName : rollcall
 * Package : com.enboyd.rollcall.dao
 *
 * @author : LTY
 * @date : 2018/12/17 11:39
 * Description :
 */
public class StudentDaoTest {

    @Autowired
    private StudentDao studentDao;

    @Test
    public void list() {
        Student student = new Student();
        student.setClassName("6-1");
        List<Student> studentList = studentDao.list(student);
        Assert.assertNotNull(studentList);
    }

    @Test
    public void save() {
    }
}