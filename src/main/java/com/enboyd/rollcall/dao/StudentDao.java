package com.enboyd.rollcall.dao;

import com.enboyd.rollcall.entity.Student;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ProjectName : rollcall
 * Package : com.enboyd.rollcall.dao
 *
 * @author : LTY
 * @date : 2018/12/17 11:27
 * Description :
 */
@Mapper
@Repository
public interface StudentDao {

    @Select("select * from student ")
    List<Student> list(Student student);

    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into student(name, number, gender, className) values(#{name}, #{number}, #{name}, #{gender}, #{level}, #{className})")
    Long save(Student student);

    @Select("select * from student where className = #{className} order by id")
    List<Student> listByClass(String className);
}
