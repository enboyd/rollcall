package com.enboyd.rollcall.service;

import com.enboyd.rollcall.entity.Student;

import java.util.List;

/**
 * ProjectName : rollcall
 * Package : com.enboyd.rollcall.service
 *
 * @author : LTY
 * @date : 2018/12/17 11:34
 * Description :
 */
public interface StudentService {

    List<Student> list(Student student);

    List<Student> listByClass(String className);

    Long save(Student student);
}
