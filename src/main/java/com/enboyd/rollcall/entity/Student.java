package com.enboyd.rollcall.entity;

import lombok.Data;

import javax.annotation.Nullable;

/**
 * ProjectName : rollcall
 * Package : com.enboyd.rollcall.entity
 *
 * @author : LTY
 * @date : 2018/12/17 11:14
 * Description :
 */
@Data
public class Student {

    private long id;

    private String name;

    private String number;

    @Nullable
    private String gender;

    private String className;


}
