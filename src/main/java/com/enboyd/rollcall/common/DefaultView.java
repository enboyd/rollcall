package com.enboyd.rollcall.common;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * ProjectName : rollcall
 * Package : com.enboyd.rollcall.common
 *
 * @author : LTY
 * @date : 2018/12/17 10:53
 * Description :
 */
@Configuration
public class DefaultView implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry registry){
        registry.addViewController("/").setViewName("forward:/students.html");
//        registry.addViewController("/rollcall").setViewName("forward:/students.html");
    }
}
