package com.enboyd.rollcall.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.enboyd.rollcall.entity.Student;
import com.enboyd.rollcall.service.StudentService;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * ProjectName : rollcall
 * Package : com.enboyd.rollcall.controller
 *
 * @author : LTY
 * @date : 2018/12/17 10:49
 * Description :
 */
@RequestMapping("/rollcall")
@RestController
public class RollCallController {

    @Autowired
    private StudentService studentService;

    @RequestMapping(value = "{className}")
    public JSONObject getStudentByClass(@PathVariable("className") String className) {
        List<Student> studentList = studentService.listByClass(className);
        JSONArray array = JSONArray.parseArray(JSON.toJSONString(studentList));

        JSONObject json = new JSONObject();
        json.put("code", 0);
        json.put("msg", "成功");
        json.put("count", array.size());
        json.put("data", array);
        return json;
    }

}
