package com.enboyd.rollcall.service.impl;

import com.enboyd.rollcall.dao.StudentDao;
import com.enboyd.rollcall.entity.Student;
import com.enboyd.rollcall.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ProjectName : rollcall
 * Package : com.enboyd.rollcall.service.impl
 *
 * @author : LTY
 * @date : 2018/12/17 11:35
 * Description :
 */
@Service
@Slf4j
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentDao studentDao;

    @Override
    public List<Student> list(Student student) {
        return studentDao.list(student);
    }

    @Override
    public List<Student> listByClass(String className) {
        return studentDao.listByClass(className);
    }

    @Override
    public Long save(Student student) {
        return studentDao.save(student);
    }
}
